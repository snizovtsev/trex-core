Name:		trex-core
Version:	2.86
Release:	0.1%{?dist}
Summary:	TRex Low-Cost, High-Speed Stateful Traffic Generator - Core

License:	ASL 2.0
URL:		https://trex-tgn.cisco.com/
Source0:	https://github.com/cisco-system-traffic-generator/trex-core/archive/v%version.tar.gz#/%name-%version.tar.gz

Patch1:		fix-paths.patch
Patch2:		disable-werror.patch

BuildRequires:	zlib-devel libmnl-devel
%if 0%{?rhel} > 7 || 0%{?fedora}
BuildRequires:	python3-devel
Requires:	python3 python3-pyyaml
%else
BuildRequires:	python2-devel
Requires:	python2 PyYAML
%endif
BuildRequires:	gcc gcc-c++
Requires:       pciutils libibverbs libmnl

# FIXME Actually trex-core embeds a specific version of openssl
AutoReqProv:	no
Requires:	bash

# t-rex uses igb_uio.ko, but we don't package it and so t-rex needs to build it
Requires: make gcc kernel-devel elfutils-libelf-devel

# FIXME Turn off the brp-python-bytecompile script
%global __os_install_post %(echo '%{__os_install_post}' | sed -e 's!/usr/lib[^[:space:]]*/brp-python-bytecompile[[:space:]].*$!!g')

%global debug_package %{nil}

%global targets _t-rex-64,_t-rex-64-o

%description
%summary


%prep
%autosetup -p1
# Fix find_python.sh PATH
grep -lrZ '[^/]find_python.sh' scripts | xargs -0 sed -i 's:\([^/]\)find_python.sh:\1%{_datadir}/%{name}/scripts/find_python.sh:g'

# Fix PYTHONPATH
grep -FlZ 'export PYTHONPATH=' scripts/* 2>/dev/null | xargs -0 sed -i 's:export PYTHONPATH=:export PYTHONPATH=%{_datadir}/%{name}/scripts/:'

# Upstream find_python.sh try to use randomly (50%) python2 or python3. This is a completely non-sense, so just make it use our python.
%if 0%{?rhel} > 7 || 0%{?fedora}
echo PYTHON=%{__python3} > scripts/find_python.sh
%else
echo PYTHON=%{__python2} > scripts/find_python.sh
%endif

%build
# FIXME
export CFLAGS="%{optflags} -Wno-error=format-security -Wno-format-security"
export CXXFLAGS="%{optflags} -Wno-error=format-security -Wno-format-security"
export LINKFLAGS="%{__global_ldflags}"
sed -i "s/'-Werror',//" linux{,_dpdk}/ws_main.py

pushd linux
./b --targets=bp-sim-64 \
    configure -v \
    --prefix=%{_prefix} \
    --libdir=%{_libdir}

./b --targets=bp-sim-64 \
    build -v %{?_smp_mflags}
popd

pushd linux_dpdk
./b --targets=%{targets} \
    configure -v \
    --prefix=%{_prefix} \
    --libdir=%{_libdir}

./b --targets=%{targets} \
    build -v %{?_smp_mflags}
popd

%install
pushd linux
./b --targets=bp-sim-64 \
    install --destdir=%{buildroot}
popd

pushd linux_dpdk
./b --targets=%{targets} \
    install --destdir=%{buildroot}
popd

# FIXME sadly upstream likes to have all the stuff in one directory :(
# Install stuff
mkdir -p %{buildroot}%{_datadir}/%{name}/scripts
for x in trex-cfg stl-sim astf-sim find_python.sh run_regression \
         run_functional_tests dpdk_nic_bind.py dpdk_setup_ports.py \
         doc_process.py trex_daemon_server general_daemon_server \
         master_daemon.py trex-console daemon_server t-rex-64; do
         
         install -m755 scripts/$x %{buildroot}%{_datadir}/%{name}/scripts/$x
done
for x in 'cap2' 'avl' 'cfg' 'ko' 'automation' 'external_libs' 'stl' 'exp' 'astf' 'so'; do
    cp -r scripts/$x %{buildroot}%{_datadir}/%{name}/scripts/
done

# FIXME they have lots of stuff with chmod +x and rpm doesn't like them
find %{buildroot}%{_datadir}/%{name}/scripts/ -mindepth 2 -type f -exec chmod -x {} +

ln -s t-rex-64 %{buildroot}%{_datadir}/%{name}/scripts/t-rex-64-o
ln -s %{_datadir}/%{name}/scripts/t-rex-64 %{buildroot}%{_bindir}/t-rex-64
ln -s %{_datadir}/%{name}/scripts/t-rex-64-o %{buildroot}%{_bindir}/t-rex-64-o

%files
%license LICENSE
%{_bindir}/bp-sim-64
%{_bindir}/_t-rex-64
%{_bindir}/_t-rex-64-o
%{_bindir}/t-rex-64
%{_bindir}/t-rex-64-o
%{_libdir}/libbpf-64.so
%{_libdir}/libbpf-64-o.so
%{_datadir}/%{name}/scripts/*


%changelog
* Thu Dec 17 2020 Sergey Nizovtsev <snizovtsev@gmail.com> - 2.86-0.1
- Version bump
- Added libmnl and libibverbs as Requires
- Disable Werror for gcc

* Thu Feb 06 2020 Sergey Nizovtsev <snizovtsev@gmail.com> - 2.73-0.1
- Version bump

* Thu Oct 10 2019 Timothy Redaelli <tredaelli@redhat.com> - 2.65-0.2
- Added PyYAML/python3-pyyaml as Requires
- Fixed PYTHONPATH
- Use our python (/usr/bin/python{2,3}) instead of relying to randomic upstream find_python.sh

* Tue Oct 08 2019 Timothy Redaelli <tredaelli@redhat.com> - 2.65-0.1
- Version bump

* Thu Sep 27 2018 Timothy Redaelli <tredaelli@redhat.com> - 2.45-0.1
- Version bump

* Wed Oct 04 2017 Timothy Redaelli <tredaelli@redhat.com> - 2.30-0.4
- Initial version, still needs improvements and fixmes

